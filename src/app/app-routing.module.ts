import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './search/search.component';
import { HistoryComponent } from './history/history.component';

import {SupportComponent} from './support/support.component'

//all the navigation path for the application
const routes: Routes = [
  {
    path: 'search', component:SearchComponent
  },
  {
    path: 'history', component:HistoryComponent
  },
  {
    path: '**', component: SupportComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
