import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }
  
  //return the list of search records for a given entry in the text box
 public getSearchedRecords(searchName: string)
 {
   return this.http.get('https://api.github.com/search/users?q='+searchName+'+repos:%3E42+followers:%3E1000');
 }
}
