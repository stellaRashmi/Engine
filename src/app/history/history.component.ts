import { Component, OnInit } from '@angular/core';
import {HistoryService} from '../history.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  historySearchlength = 0;
  historyRecords: object;
  constructor(private historyService: HistoryService) { }

  ngOnInit() {
    this.getHistorySearch();
  }
getHistorySearch()
{ 
  this.historyService.getHistorySearch().subscribe(
    (data: any)=>{
   
    this.historySearchlength = data.items.length;
    this.historyRecords = data.items;
  },
  (error) => {                             
    console.error('error in component');   
  });
}
}
