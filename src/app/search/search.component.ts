import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {SearchService} from '../search.service'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  isformSubmitted  = false;
  searchedRecords : Object;
  searchRecordlength = 0;
  constructor(private searchService: SearchService) { }

  ngOnInit() {
  }
  searchForm = new FormGroup({
    searchData: new FormControl()
  });
  onSubmit()
  {
    this.isformSubmitted = true;
    console.log(this.searchForm.get('searchData').value);
    let searchName = this.searchForm.get('searchData').value;
    
    this.searchService.getSearchedRecords(searchName).subscribe(
      (data: any)=>{
      console.log(data);
      this.searchRecordlength = data.items.length;
      this.searchedRecords = data.items;
    },
    (error) => {                             
      console.error('error in component');   
    });
  }
}
